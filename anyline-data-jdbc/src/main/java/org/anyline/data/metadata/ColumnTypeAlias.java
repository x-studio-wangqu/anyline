package org.anyline.data.metadata;

import org.anyline.entity.metadata.ColumnType;

public interface ColumnTypeAlias {
    public ColumnType standard();
}
